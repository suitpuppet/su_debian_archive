# NOTE! Update the hash $codename_to_suite in this file when there is a new
# official release!!
#
# Handles the setup and configuration of Stanford's local
# Debian repositories, e.g., the two repositories "local" and
# "stanford". However, other repositories can also be defined.
#
# This is provided in the form of a define to make it easy to manage multiple
# separate repositories using reprepro.
#
# $name: The archive name, e.g., "local" or "stanford".
#
# $ensure: one of 'present' or 'absent'. If 'absent' will REMOVE the
#   archive.
#   Default: undef
#
# $base_dir: where the archive will be put. The caller must arrange for the
#   this directory to be created if necessary.  This class just depends on
#   the file object for that directory.
#   Default: '/srv/repos'
#
# $debian_releases: a list of release codenames. Do _not_ include in this list any
#   Ubuntu releases or the "sid" or "experimental" releases.
#
# $debian_distributions: a list of Debian distributions we set up for each
#    Debian release.
#
# Example:
#
#  su_debian_archive { 'local':
#    key => 'idg-local-rep'
#  }
#
# will set up the 'local' archive in the directory '/srv/repos' (the
# default) with the key found in the wallet file object
# 'idg-local-rep-gpg-key'
#
# EXAMPLE 2.
#
#  su_debian_archive { 'local':
#   $debian_releases = [
#     'jessie',
#     'stretch',
#     'sid',
#   ]
#   $debian_distributions = [
#     'BASE',
#     'backports',
#     'kdc',
#     'ldap',
#     'acs-dev',
#   ],
#  }
#
# This will setup the following distributions:
#   jessie            (corresponds to BASE)
#   jessie-backports
#   jessie-kdc
#   jessie-ldap
#   jessie-acs-dev
#   stretch           (corresponds to BASE)
#   stretch-backports
#   stretch-kdc
#   stretch-ldap
#   stretch-acs-dev
#   stable              -> jessie
#   oldstable-backports -> jessie-backports
#   oldstable-kdc       -> jessie-kdc
#   oldstable-ldap      -> jessie-ldap
#   oldstable-acs-dev   -> jessie-acs-dev
#   stable              -> stretch
#   stable-backports    -> stretch-backports
#   stable-kdc          -> stretch-kdc
#   stable-ldap         -> stretch-ldap
#   stable-acs-dev      -> stretch-acs-dev
#   sid
#   unstable            -> sid
#
# $pulls: a hash of arrays. Example:
#
# $pulls => {
#    'stable' => [
#      'filter-syslog',
#      'kpasswd-web',
#      'stanford-webauth'
#    ],
#    'oldstable' => [
#      'stanford-hello-debian',
#      'directory-regression-testing',
#      'stanford-data-access-request'
#    ]
# }
#
# will create the two files /srv/repos/$name/conf/pull-stable and
# /srv/repos/$name/conf/pull-oldstable with the contents
#
#   # /srv/repos/$name/conf/pull-stable
#   filter-syslog     install
#   kpasswd-web       install
#   stanford-webauth  install
#
# and
#
#   # /srv/repos/$name/conf/pull-oldstable
#   stanford-hello-debian         install
#   directory-regression-testing  install
#   stanford-data-access-request  install

define su_debian_archive (
  String $ensure        = undef,
  Optional[String] $key = undef,
  String $base_dir      = '/srv/repos',
  String $group         = 'root',
  #
  Boolean $enable_auto_pull = false,
  #
  $debian_releases = [
    'wheezy',
    'jessie',
    'stretch',
    'buster',
    'sid',
  ],
  $debian_distributions = [
    'BASE',
    'backports',
    'afs',
    'email',
    'kdc',
    'ldap',
    'acs-dev',
    'acs-test',
    'acs-uat',
    'acs-prod',
    'email-dev',
    'email-test',
    'email-uat',
    'email-prod',
    'heimdal-dev',
    'heimdal-test',
    'heimdal-uat',
    'heimdal-prod',
    'openldap-dev',
    'openldap-test',
    'openldap-uat',
    'openldap-prod',
    'ossec-dev',
    'ossec-test',
    'ossec-uat',
    'ossec-prod',
  ],
  $debian_architectures = [
    'amd64',
    'i386',
    'source',
  ],
  #
  $ubuntu_releases = [
    'saucy',
    'trusty',
    'xenial',
    'bionic',
    'focal',
  ],
  $ubuntu_distributions = [
    'BASE',
    'backports',
    'mailman-dev',
    'mailman-test',
    'mailman-uat',
    'mailman-prod',
  ],
  $ubuntu_architectures = [
    'amd64',
    'source',
  ],
  #
  Hash $pulls = {},
) {
  include su_debian_archive::base

  if !($ensure in [ 'present', 'absent' ]) {
    fail("ensure must be present or absent, not ${ensure}")
  }

  # Update this when there is a new official release!
  $codename_to_suite = {
    'wheezy'       => 'oldoldoldoldoldstable',
    'jessie'       => 'oldoldoldoldstable',
    'stretch'      => 'oldoldoldstable',
    'buster'       => 'oldoldstable',
    'bullseye'     => 'oldstable',
    'bookworm'     => 'stable',
    'trixie'       => 'testing',
    'sid'          => 'unstable',     # This never changes.
    'experimental' => 'experimental', # This never changes.
  }

  $debian_distribution_descriptions = {
    'BASE'      => 'Stanford supplemental Debian repository',
    'backports' => 'Stanford supplemental Debian package backports',
    'afs'       => 'AFS repository for Stanford UIT-specific Debian packages',
    'email'     => 'Repository for Stanford UIT-specific Debian Email packages',
    'kdc'       => 'Repository for Stanford UIT-specific Debian KDC packages',
    'ldap'      => 'Repository for Stanford UIT-specific Debian OpenLDAP packages',
    'acs-dev'   => 'Repository for Stanford ACS Team-specific Debian packages (DEV)',
    'acs-test'  => 'Repository for Stanford ACS Team-specific Debian packages (TEST)',
    'acs-uat'   => 'Repository for Stanford ACS Team-specific Debian packages (UAT)',
    'acs-prod'  => 'Repository for Stanford ACS Team-specific Debian packages (PROD)',
  }

  $ubuntu_distribution_descriptions = {
    'BASE'      => 'Stanford UIT local Ubuntu repository',
    'backports' => 'Stanford UIT local Ubuntu repository',
  }

  case $ensure {
    'present': {
      File { require => File[$base_dir] }

      file { "${base_dir}/${name}":
        ensure  => directory;
      }

      $subdirs1 = ['dists', 'db', 'pool']
      $subdirs1.each |$subdir| {
        file{ "${base_dir}/${name}/${subdir}":
          ensure  => directory,
          group   => $group,
          mode    => '02775',
          require => File["${base_dir}/${name}"],
        }
      }

      $subdirs2 = ['incoming', 'tmp']
      $subdirs2.each |$subdir| {
        file{ "${base_dir}/${name}/${subdir}":
          ensure  => directory,
          group   => $group,
          mode    => '02770',
          require => File["${base_dir}/${name}"],
        }
      }

      # conf/ is special as we populate it with files from this
      # module. Note that we put _other_ files in this same directory, so
      # we need to set purge to false.
      file{ "${base_dir}/${name}/conf":
        ensure  => directory,
        recurse => true,
        purge   => false,
        source  => "puppet:///modules/su_debian_archive${base_dir}/${name}/conf";
      }

      # Install the "distributions" file
      file { "${base_dir}/${name}/conf/distributions":
        content => template("su_debian_archive/distributions.erb"),
        require => File["${base_dir}/${name}/conf"],
      }

      # Install the "incoming" file
      file { "${base_dir}/${name}/conf/incoming":
        content => template("su_debian_archive/incoming.erb"),
        require => File["${base_dir}/${name}/conf"],
      }

      # Install the "pulls" files
      $pulls.each |$suite, $package_names| {
        su_debian_archive::pull { "${name}-${suite}":
          archive_name  => $name,
          suite         => $suite,
          base_dir      => $base_dir,
          package_names => $package_names,
        }
      }

      # We need symbolic links: one for each Debian suite/distribution.
      # Note that some (or even many) of the symbolic links may point to
      # non-existent directories. The target of the link will get created
      # the first time a package is uploaded or copied into the target
      # distribution.
      #
      # For the unstable suite we make only the link unstable -> sid.
      $debian_releases.each |$debian_release| {
        if ($debian_release == 'sid') {
          file { "${base_dir}/${name}/dists/unstable":
            ensure  => link,
            target  => "sid",
            require => File["${base_dir}/${name}"],
          }
        } else {
          $debian_distributions.each |$debian_distribution| {
            $codename = $codename_to_suite[$debian_release]
            if ($debian_distribution == 'BASE') {
              $link_name   = $codename
              $target_name = $debian_release
            } else {
              $link_name   = "${codename}-${debian_distribution}"
              $target_name = "${debian_release}-${debian_distribution}"
            }

            file { "${base_dir}/${name}/dists/${link_name}":
              ensure  => link,
              target  => $target_name,
              require => File["${base_dir}/${name}"],
            }
          }
        }
      }

      # If a signing key was specified, set up the keyring.
      #
      # Example:
      #   /srv/storage/repos/local/keyring
      #   /srv/storage/repos/local/keyring/key.asc
      if ($key) {
        $keyring = "${base_dir}/${name}/keyring"
        $keyfile = "${keyring}/key.asc"

        # We only want the root user to be able to access this directory
        # or to be able to start a gpg-agent pointing to this directory.
        file { $keyring:
          ensure => directory,
          mode   => '0770',
        }
        base::wallet { "${key}-gpg-key":
          path    => $keyfile,
          type    => 'file',
          require => File[$keyring],
        }

        # For Debian stretch and later, we use gnupg version 2 which
        # creates a directory for the secret keys.
        if ($::lsbdistcodename == 'jessie') {
          $creates_file = "${keyring}/secring.gpg"
        } else {
          $creates_file = "${keyring}/private-keys-v1.d"
        }

        exec { "gpg --import ${keyfile}":
          command => "gpg --homedir ${keyring} --import ${keyfile}",
          creates => $creates_file,
          require => Base::Wallet["${key}-gpg-key"],
        }
        file {
          [
            "${keyring}/pubring.gpg",
            "${keyring}/secring.gpg",
            "${keyring}/trustdb.gpg"
          ]:
          group   => $group,
          mode    => '0640',
          require => Exec["gpg --import ${keyfile}"];
        }
      }
    }

    default: {
      file { "${base_dir}/${name}":
        ensure  => $ensure,
        recurse => true,
      }
    }
  }
}
