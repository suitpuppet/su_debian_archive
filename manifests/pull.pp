# $name: usually the archive followed by the suite  (e.g., "local-stable", "stanford-oldstable")

define su_debian_archive::pull (
  String        $archive_name  = undef,
  String        $suite         = undef,
  String        $base_dir      = undef,
  Array[String] $package_names = [],
) {
  if (empty($package_names)) {
    file { "${base_dir}/${archive_name}/conf/pull-${suite}":
      ensure => absent
    }
  } else {
    file { "${base_dir}/${archive_name}/conf/pull-${suite}":
      ensure  => present,
      content => template('su_debian_archive/pull.erb'),
    }
  }
}
