# This class
#  * ensures that the reprepro package is installed
#  * enables the reprepo remctl interface
#  * installs the cron job to back up the repositories
class su_debian_archive::base (
  Array[String] $repo_uploaders = [],
) {
  package { 'reprepro': ensure => installed }

  # REMCTL REPREPRO CONFIG
  # remctl backend for reprepro modifications and uploads.
  file { '/etc/remctl/conf.d/reprepro':
    content => template("su_debian_archive/etc/remctl/conf.d/reprepro.erb"),
  }

  # REMCTL ACL FILES

  # repo-uploaders are those allowed to upload to the debian-repo servers.
  #
  # The post_upload_command in .dput.cf should be set to
  # "remctl debian-repo.stanford.edu reprepro-upload <archive>", e.g.,
  # "remctl debian-repo.stanford.edu reprepro-upload stanford" or
  # "remctl debian-repo.stanford.edu reprepro-upload local".
  remctl::server::aclfile { 'repo-uploaders':
    ensure => present,
    acls   => $repo_uploaders,
  }

  # The file /etc/remctl/acl/debian-repo-managers is managed directly by
  # the remctl aclfile service (https://code.stanford.edu/et-iedo/cloud/kube-aclfile)
  # so we do not need to manage it here.

  # REPREPRO BACKUP
  # Install nightly backup cron job, but only in production.
  if ("$s_debianrepo::env" == 'prod') {
    file { '/etc/cron.daily/repo-backup':
      ensure => present,
      source => 'puppet:///modules/su_debian_archive/etc/cron.daily/repo-backup',
    }
  } else {
    file { '/etc/cron.daily/repo-backup':
      ensure => absent,
    }
  }

}
