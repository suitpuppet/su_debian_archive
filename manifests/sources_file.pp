# Create a /etc/apt/sources.list.d/ list file
#
# Assumes that the repository is one of these two:
#
#   http://debian.stanford.edu
# or
#   http://debian-dev.stanford.edu
#
#
# Example:
#  su_debian_archive::sources_file { 'stanford-stable-acs-distros':
#    archive       => 'stanford',
#    distributions => [
#      'stable-acs-dev',
#      'stable-acs-test',
#      'stable-acs-prod',
#    ],
#    description   => 'The ACS Team stable distributions (dev, test, and prod)',
#  }
#
# will generate the file
# /etc/apt/source.list.d/stanford-stable-acs-distros.list with this
#  content:
#
#  # The ACS Team stable distributions (dev, test, and prod)
#
#  deb     http://debian.stanford.edu/debian-stanford stable-acs-dev main non-free contrib
#  deb-src http://debian.stanford.edu/debian-stanford stable-acs-dev main non-free contrib
#
#  deb     http://debian.stanford.edu/debian-stanford stable-acs-test main non-free contrib
#  deb-src http://debian.stanford.edu/debian-stanford stable-acs-test main non-free contrib
#
#  deb     http://debian.stanford.edu/debian-stanford stable-acs-prod main non-free contrib
#  deb-src http://debian.stanford.edu/debian-stanford stable-acs-prod main non-free contrib

define su_debian_archive::sources_file (
  Enum['present', 'absent']
                $ensure         = undef,
  Enum['local', 'stanford', 'tcg']
                $archive        = undef,
  Array[String] $distributions  = [],
  String        $description    = undef,
) {

  file { "/etc/apt/sources.list.d/${name}.list":
    ensure  => $ensure,
    content => template("su_debian_archive/etc/apt/sources.list.d/file.erb"),
    notify  => Exec["sources_file_apt_update_${name}"],
  }

  exec { "sources_file_apt_update_${name}":
    path        => '/usr/bin:/bin:/usr/sbin',
    command     => 'apt-get update',
    refreshonly => true,
  }

}
